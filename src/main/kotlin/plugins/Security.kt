package org.nexa.debug

import io.ktor.server.sessions.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.request.*
import io.ktor.server.routing.*

fun Application.configureSecurity()
{
    install(Sessions)
    {
        cookie<NdbSessionId>("NDB")
        {
            cookie.path = "/"
            cookie.extensions["SameSite"] = "lax"
        }
    }

    /*
    routing {
        get("/session/increment") {
                val session = call.sessions.get<NdbSessionId>() ?: MySession()
                call.sessions.set(session.copy(count = session.count + 1))
                call.respondText("Counter is ${session.count}. Refresh to increment.")
            }
    }
     */
}
